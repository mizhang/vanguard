\section{Evaluation of preliminary prototype}
\label{sec:eval}
%
In this section, we describe an initial evaluation of our \vanguard gateway which
serves as a proxy for all communication with the Docker daemon.
% As stated in Section~\ref{design}, our preliminary prototype of \vanguard has been implement
% as a service proxying all communication to the Docker daemon.
%
In our effort to assess our current prototype, we perform the following steps: 
1) we study the runtime overhead added to Docker operations by the
proxy, and confirm that \vanguard transparently proxies commands even
when they are issued by third-party tools, such as Kubernetes, 
2) we verify the effectiveness of the prototype
in stopping attempts to break the enforced policy, and 
3) we attempt to
quantify the impact of \vanguard's presence on addressing  a specific class of potential threats
related to Docker entry point misuse.


\subsection{Runtime performance}
%
The most crucial \vanguard policies are checked and enforced during Docker container launch operations.
We used a particularly popular Docker container image---the official
\code{MySQL} image available on the Docker registry---and a simple
policy in order to measure the overhead that \vanguard introduces by
proxying communication with the Docker daemon.
%
Our testbed is a Linux x86-64 Ubuntu 14.04 System running on 
an Intel Core-i5 4200 CPU, with 12 Gigabytes of RAM and an 128GB SSD.
We measured the impact on launch time both when launching a single
container and when launching up to fifty containers from the image
simultaneously.
%
Figure~\ref{fig:startup} shows that the median overhead introduced by \vanguard 
is 11.2\%, which we find acceptable. The policy we are using for this experiment 
is very simple. It contains only two rules, of which the first requires that the 
container should not be started in privileged mode and the second requires that 
the data mapped into the container can only be at location {\tt
  /home/public}. More complicated policies increase the overhead somewhat,
as shown in Table~\ref{fig:policy_parse_time}, but keep in mind that 
this overhead applies only to container management operations and that
these times reflect the performance of our unoptimized prototype.  
\vanguard does 
not introduce any overhead to the runtime performance of the contained 
application.  
%
\begin{figure}
 \begin{center}
  \includegraphics[width=0.48\textwidth]{figs/startup}
 \end{center}
 \caption{\vanguard container launch time overhead.}
 \label{fig:startup}
\end{figure}
%
\begin{table}
 \caption{ \label{fig:policy_parse_time} \vanguard policy parsing and evaluation performance.}
\begin{center}
 \begin {tabular}{|r|r|}
\hline
{\bf Number of Rules} & {\bf Time to Evaluate}\\
\hline
\hline
5&0.07 sec\\
\hline
50&0.1 sec\\ 
\hline
10,000&0.5 sec\\
\hline
100,000&6 sec\\
\hline
\end{tabular}
\end{center}
\end{table}

% -- runtime 10+ major applications 
 %\subsection{Integration with Kubernetes}
\paragraph{Integration with Kubernetes.} We have also tested \vanguard with a popular container management system, 
Kubernetes, which we described in Section~\ref{sec:background}. In particular, we tested our system in the Google 
container cloud using a small deployment consisting of two VMs: a master 
and a slave node, both protected by \vanguard. We exercised the system through
Kubernetes for the duration of a week, successfully testing all Kubernetes' sample
applications (pods) in \code{kubernetes/examples/guestbook}, with no
issues.  This simple experiment demonstrates that our prototype
successfully parses the Docker Remote API commands
 whether they are issued directly by the Docker client, or by a third-party tool such as
Kubernetes.  This ability to expose the same interface as the Docker
daemon and to transparently proxy all commands allows \vanguard to
seamlessly integrate with the Docker ecosystem.

\subsection{\vanguard policy enforcement scenarios.}
\label{sec:app}

\begin{table*}[h]
\caption{Unintended behavior prevented by \vanguard}
\label{tab:app}
\centering
\begin{small} 
\begin{tabular}{|l|l|c|}
\hline
\multicolumn{1}{|c|}{\textbf{Unintended Behavior}}
& \multicolumn{1}{c|}{\textbf{Action}} & \multicolumn{1}{c|}{\textbf{Policy Type}} \\ \hline
\texttt{docker run pull unauthorized\_img}                                                                                     & Reject                        & IOP                         \\ \hline
%docker tag unauthorized -t authorized                                                                                & Reject                        & IOP                         \\ \hline
\texttt{docker rmi alice\_image}                                                                                              & Reject                        & IOP                         \\ \hline
\texttt{docker rm alice\_container}                                                                                           & Reject                        & COP                         \\ \hline
\texttt{docker run -v/data/alice:/data cary/ftp scp /data myweb}        & Reject                        & COP                         \\ \hline
\texttt{docker run -v/etc/shadow:/tmp/pass ubuntu scp /tmp/pass cary\_web}   & Reject                        & COP                         \\ \hline
% \begin{tabular}[c]{@{}l@{}}\texttt{docker run -v/data/alice:/data cary/ftp \textbackslash}\\  \texttt{scp /data myweb}\end{tabular}        & Reject                        & COP                         \\ \hline
% \begin{tabular}[c]{@{}l@{}}\texttt{docker run -v/etc/shadow:/tmp/pass \textbackslash}\\ \texttt{ubuntu scp /tmp/pass cary\_web}\end{tabular}   & Reject                        & COP                         \\ \hline
%\begin{tabular}[c]{@{}l@{}}docker run -v/etc/shadow:/tmp/pass \textbackslash\\  ubuntu scp /tmp/pass alice\_web\end{tabular}   & Reject                        & COP                         \\ \hline
\end{tabular}
\end{small} 
\end{table*}

To evaluate the effectiveness of \vanguard, we evaluated a
hypothetical scenario involving an organization with several users
sharing a single Docker container host which is administered by a
developer \code{Alice}.  Our organization has another member \code{Cary}
who is a developer in a different group, and \code{Bob} who is a tester
in the QA group.  We simulated a
scenario where \code{Alice} builds a new containerized application and
wants to allow \code{Bob} to access her host to test the application 
and to grant \code{Cary} access to her host in order to experiment with
Docker and build her own containerized
applications.  \code{Alice} doesn't want either of the others to have access to the
private data located on her container host.

We found that \vanguard is able to effectively enforce policy in this
scenario.  In Table~\ref{tab:app}, we list several unauthorized
commands that \code{Cary} attempts on \code{Alice}'s host, but
that \vanguard is able to reject based on the policy \code{Alice} has
put in place.  For instance, \code{Cary}'s attempts to download an
unauthorized image is rejected, as is her attempt to copy the file with {\tt
  Alice}'s passwords.  Note that even if we trust Cary not to act
maliciously, the principle of least privilege dictates that she should
be prevented from performing sensitive actions that she doesn't need to.
That way, even if Cary's account is compromised by a malicious
user, the damage is contained.  \vanguard allows the implementation of
such a policy on a container host.
% (We'll give \code{Cary} the benefit of the doubt
% and assume that a malicious user stole her credentials and issued
% the command attempting to steal \code{Alice}'s password. It's a good
% thing \code{Alice} used \vanguard to implement the principle of least
% privilege.)



% In the experiment, {\tt Cary} issues several unintended Docker commands, all of 
% them are prohibited by the policy enforced by \vanguard, that are shown in 
% Table~\ref{tab:app}. For instance, Cary will be rejected when she tries to use 
% an unauthorized image, since she violates the current IOP policy.
%Similarly, she could rename an unauthorized image to an authorized one and 
%launch it. This is again is rejected by IOP violation. In addition, this normal 
%user does not have privilege of deleting unauthorized images.

% With respect to container operations, Cary is able to perform arbitrary operations on his own 
% containers, but holds no privilege over other users' containers---and therefore 
% the third command in Table~\ref{tab:app} is rejected.
% %
% Similarly, when it comes to launching containers, Cary is not allowed to launch unintended 
% binaries or map unauthorized resource to his containers. Therefore, behaviors in rows 5 
% and 6 are rejected.
% \comment{
% In the example of the last row, we address a more subtle case of unintended behavior: 
% let us assume Cary creates a new image based on the authorized image {\tt cary/redis}, 
% but attempts to replace the entry point binaries as well as some crucial configuration files by
% using her own, externally mapped, versions.
% %
% Even though the new image may appear as ``authorized'' due to policy 
% inheritance, \vanguard prevents this image from running since {\tt Cary} violate 
% the resource mapping rule in the policy..
% }

%The last case is a little bit tricky. Cary is trying to leverage the policy 
%inheritance to bypass \vanguard policies. She generates a new image from an 
%authorized image {\tt cary/redis}. However, she totally changed the base image 
%by modifying the entrypoint binaries with her own and configurations and etc. 
%Now, she mount important from host using this new image. This behavior is still 
%rejected in the 2nd step despite the fact of policy inheritance 
%(Section~\ref{sec:iop}). This is because resource mapping on data cannot 
%be inherited.

\subsection{Automatic Docker image analysis}
\label{sec:img-anal}
%
To better understand the potential impact of \vanguard and the opportunity 
to enforce policies that prevent undesirable behaviors, we used the Docker registry
API~\cite{docker-reg-API} in order to analyze a large number of 
Docker images publicly available in the Docker hub registry~\cite{docker-hub}.
The goal of our analysis was to assess the degree to which these images 
contain configuration oversights that may lead to unintended behaviors. 
%
In particular, for the purposes of this paper, we will focus on our findings related
container entry point specification, or lack thereof.\footnote{Note that we also made 
other potentially threatening observations, such as 2280 application passwords in plain text 
hard-coded in the images, that could be addressed by \vanguard, but are beyond the scope
of our current prototype's policy language.}

We crawled 84,897 Docker repositories (images) from 24,066 users by recursively calling the 
API query functions (similar to \code{docker search}). 
This corresponds to the majority of the all repositories in the public Docker registry, 
including 72 official repositories as well as 31,790 automatically built 
repositories connected with \code{github}. 

\comment{
We have several interesting 
discoveries by investigating these docker images.
\paragraph{Passwords in plain text} After retrieving the metadata of all docker 
repositories, we have found 2280 hard coded passwords in plain text. 98.6\% of 
these passwords are weak in strength. These plain text passwords are located in 
the metadata of docker images. These passwords are needed to setup proper user 
account permissions. \vanguard is able to change the default passwords when a 
container is launched from the image. However, there will be no guarantee of 
working if the inner user program assumes the original password when switching 
user ID.

Therefore, the fundamental solution is on the developer site. We suggest that 
developers do not pass their pass codes in command line, but put them in file 
and redirect to command {\\tt chpasswd}, when building a docker image. 
Alternatively, putting passwords into environment variables before feeding them 
into \code{chpasswd} is even safer since there is no plain text passwords 
anywhere inside image. Unfortunately, only 100 repositories uses the approach. 
On default, \vanguard will reject images that have explicit password setup to 
run, since they might easily be controlled by malicious developer or any 
attackers that are aware of this vulnerable design in Docker.
}
\paragraph{Entry point analysis.} Of the 84,897 images that we
analyzed, 12,845 (15.1\%) of the images have an explicit entry point
specified in their metadata.  These images were generated from a
Dockerfile containing an \code{ENTRYPOINT} statement.  The rest have a
default entry point specified in their metadata 
which will only be used if another command is not specified on the
\code{docker run} command line.  These images were built from a
Dockerfile containing a \code{CMD} statement.

The existence of an \code{ENTRYPOINT} in the metadata
is a strong indication that the container is intended to run
only the specified command.  \vanguard can be used to ensure that
users do not use special Docker command line switches to override this
intended entry point.    
% The key difference between {\tt CMD} and 
% {\tt ENTRYPOINT} is that the latter enforces the startup program of a container, 
% startup command in {\tt docker run} operation are regarded as parameters of this 
% program. In compare, when {\tt ENTRYPOINT} is not specified, {\tt CMD} 
% represents the default startup program, which can be replaced by startup commands provided by user.
Tables~\ref{fig:entry} and~\ref{fig:cmd} 
demonstrate the top-10 entry points as indicated by \code{ 
ENTRYPOINT} and \code{CMD} specifications respectively.
\begin{table}[h]
 \caption{Top-10 entry points (for containers with no entrypoint statements)}
 \label{fig:entry}
 \begin{center}
\begin{small} 
\begin{tabular}{|l|r|}
\hline
\textbf{Entry point}                & \textbf{Number} \\ \hline
/entrypoint.sh            & 780    \\ \hline
/docker-entrypoint.sh     & 347    \\ \hline
/start.sh                 & 179    \\ \hline
/run.sh                   & 172    \\ \hline
/usr/local/bin/jenkins.sh & 165    \\ \hline
/bin/bash                 & 151    \\ \hline
/bin/sh -c usr/bin/mongod & 140    \\ \hline
/usr/bin/redis-server     & 132    \\ \hline
/app/init                 & 107    \\ \hline
/usr/sbin/apache2         & 75     \\ \hline
total                     & 12845  \\ \hline
\end{tabular}
\end{small} 
 \end{center}
\end{table}

\begin{table}[h]
\begin{center}
\caption{Top-10 default executables (for containers with no
  entrypoint statement)}
 \label{fig:cmd}
\begin{small} 
\begin{tabular}{|l|r|}
\hline
\textbf{Default startup}      & \textbf{Number} \\ \hline
/bin/bash            & 9723   \\ \hline
/sbin/my\_init       & 2111   \\ \hline
run.sh               & 1244   \\ \hline
bash                 & 1015   \\ \hline
/usr/bin/supervisord & 866    \\ \hline
/bin/sh              & 735    \\ \hline
nginx -g daemon off  & 536    \\ \hline
start.sh             & 482    \\ \hline
/usr/sbin/sshd -D    & 365    \\ \hline
/sbin/init           & 350    \\ \hline
total                & 71528  \\ \hline
\end{tabular}
\end{small} 
\end{center}
\end{table}

In Table~\ref{fig:entry} we observe that 151 images (0.9\% of all images) have entry point programs such as\code{/bin/bash} or \code{/bin/sh}.
The protection afforded by enforcing these shell entry points is of
limited value, since users can launch any shell command in interactive mode. 
But, one could imagine identifying these images and either forbidding
them altogether, or simply enforcing a policy that sensitive data can
never be mounted to containers launched from these images.

Furthermore, when looking at images with no explicit entry point
specified, we identify more opportunities for useful policy enforcement by \vanguard.
In particular, 73,386 (87\%) of these images specify meaningful (non-shell)
default programs in their \code{CMD} statement.  Again, \vanguard
could be used to automatically 
enforce that only the specified default program may be run inside the container.
% The fraction of these images that have interactive default programs such as {\tt 
% /bin/bash}, as shown in Table~\ref{fig:cmd}, require administrators to 
% manually setup policy for entry point confinement.
%\\ \TODO{Maybe we need to be more clear about the difference between entry points and CMDs. What is it? I think this is a little confusing...}

\comment{
\paragraph{Sample container launch operations} Another important source of image 
information is the description page in docker public registry. This per-image 
web page contains instructions of how to start a container from the image. 
Sample commands in these pages not only helps \vanguard discovering entry point 
programs, but also helps figuring out valid target data locations and ports 
inside containers.

In sum, we crawled all the web pages in docker public registry website. We have 
gathered  16859  \code{docker run} commands out of 9863 images.
%
%
}
