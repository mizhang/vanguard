all:
	pdflatex main
	bibtex main
	pdflatex main
	pdflatex main

clean:
	rm -f *.dvi *.bbl *.out *.dvi *.pdf *.log *.aux *.blg
