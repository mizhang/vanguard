%
\section{\vanguard Design}\label{sec:design}
%
\vanguard protects container hosts by enforcing fine-grained
policies on the container management commands issued by users.  It
accomplishes this by {\em proxying} all commands destined for the
Docker daemon and evaluating them for policy compliance before passing
them along.  In this section we describe the
design of \vanguard's architecture and the types of policies it can enforce.


\subsection{\vanguard system architecture}

The basic system architecture is shown in
Figure~\ref{fig:vanguard_arch}.  Container users issue commands to the
\vanguard gateway which, in turn, issues commands to the Docker
daemon.  The \vanguard gateway completely mediates all communication
with the Docker daemon; no other user or process is given a
communication channel to the daemon. \footnote{Preventing users from directly
  accessing the daemon can be done in a variety of ways.  In our
  prototype, we bind the Docker daemon to a unix domain socket and
  restrict access with file permissions. }  

From the point of view of the user, the \vanguard gateway exposes
exactly the same interface as the Docker daemon.  We chose this
proxy-based architecture in order to ensure that \vanguard would
easily fit into the growing Docker ecosystem.  Whether Docker Remote
API commands are issued directly by a user, or by a third-party
orchestration tool such as Kubernetes, our gateway transparently
proxies the commands and ensures policy compliance.

In our prototype implementation, policies are stored locally on the
machine that hosts the \vanguard gateway, but we envision an
architecture where policies for many container hosts are stored on a
central policy server.  Individual gateway instances would contact the
policy server to request applicable policies.  In our prototype, an
administrator is responsible for defining all aspects of the policy
for all images.  But, we envision that in a more complete
implementation, non-admin users could be granted the
privilege to adjust certain aspects of the policy for new images 
that they create.

\begin{figure*}
 \begin{center}
  \includegraphics[width=0.85\textwidth]{figs/harbormaster_arch}
 \end{center}
 \caption{Architecture of \vanguard}
 \label{fig:vanguard_arch}
\end{figure*}

% \subsection{\vanguard Policy Description}
% \begin{table}[h]
% \small
% \begin{tabular}{||l|l|l||}
% \hline
% \multicolumn{1}{||c|}{Privilege} & \multicolumn{1}{c|}{Primitive} & 
% \multicolumn{1}{c||}{Policy} \\ \hline
% \hline
% download & pull & download/execute authorized images \\ \hline
% build & build & \begin{tabular}[c]{@{}l@{}}base image is authorized;\\ new name subject to policy for tag\end{tabular} \\ \hline
% rename & tag & \begin{tabular}[c]{@{}l@{}}source name must be authorized,\\ target name preceded by username\end{tabular} \\ \hline
% remove & rmi & not allowed \\ \hline
% \begin{tabular}[c]{@{}l@{}}ctnr remove\\ cntr stop\end{tabular} & rm/stop & operate on its own containers \\ \hline
% ctnr run & run/start & \begin{tabular}[c]{@{}l@{}}execute intended entrypoint(s);\\ map data from authorized directory\end{tabular} \\ \hline
% \end{tabular}
% \caption{\vanguard Enforced Container Policy}
% \label{tab:policy}
% \end{table}

% \vanguard enforces policies on various granularity. For instance, it could 
% enforce privileges on certain users such as preventing users to generate new 
% images, or pull images from remote registry. In addition, it could also enforce 
% a per user/image policy. For instance, it does not allow Cary to execute 
% arbitrary binaries to launch a container, but allows Bob to do so.

% In sum, each user in \vanguard has her own policies as shown in 
% Table~\ref{tab:policy}. Basically, it demonstrates that each normal user in 
% \vanguard could access (download) a list of images defined by administrator. And 
% they could build new images from this image list. They could rename them as long 
% as they follow the name format policy. They cannot delete images or remove 
% containers run by other uses. But they could launch arbitrary containers from 
% the images allowed and image they built. However, any containers must map only 
% allowed locations in host and launch intended entrypoint programs from 
% authorized  images.

\newcommand{\spec} [1] {{\it $\langle$#1$\rangle$}}

\subsection{\vanguard Policies}

\begin{figure*}
\begin{tabular}{|rcl|}
\hline
\multicolumn{3}{|c|}{\bf Policy Rules}\\
&&\\
{\it Image Operation Rule} &:=& {\sc [dis]allow} {\it $\langle$user\_spec$\rangle$}  {\it $\langle$image\_action$\rangle$}
                    {\it $\langle$image\_spec$\rangle$}\\
{\it Container Operation Rule} &:=& {\sc [dis]allow} {\it $\langle$user\_spec$\rangle$} {\it
                    $\langle$container\_action$\rangle$} {\sc for} {\it $\langle$container\_spec$\rangle$}\\
\hline
\multicolumn{3}{|c|}{\bf Policy Objects}\\
&&\\
{\it user\_spec}&:=&Linux user name or group name or {\sc all}\\
{\it image\_spec}&:=&Docker repository name plus a regular expression for name matching\\
{\it container\_spec}&:=&regular expression matching a container
                          name, or an {\it $\langle$image\_spec$\rangle::\langle$user\_spec$\rangle$} which\\
&&will match all containers launched from a matching image by a matching user\\
{\it file\_spec}&:=&Linux pathname with regular expression support\\
{\it port\_spec}&:=&list of port ranges or {\sc all}\\
{\it capability\_spec}&:=&list of Linux capabilities (e.g., {\sc
                           cap\_net\_raw}) or {\sc all}\\
{\it resource\_constraint\_spec}&:=&a condition ($<$,$>$,==) on any of the
                             resource weighting priorities supported
                              \\
&&by Docker (e.g., CPU usage, memory usage,
                             I/O bandwidth)\\
\hline
\multicolumn{3}{|c|}{\bf Image Policy Action ({\it
  image\_action}) Descriptions}\\
&&\\
{\sc pull}&&download and store an image\\
{\sc extend from}&&create a new image using the specified image as a base\\
{\sc remove}&&delete an image\\
\hline
\multicolumn{3}{|c|}{\bf Container Policy Action ({\it
  container\_action}) Descriptions}\\
&&\\
{\sc map host file}&& takes a \spec{file\_spec} as an argument and
                      allows user to launch\\
&&the specified container with access to
                      the given host file(s)\\
{\sc map host port}&&takes a \spec{port\_spec} as an argument and
                      allows user to launch\\
&&the specified container with access to
                      the given host port(s)\\
{\sc grant capability} && takes a \spec{cap\_spec} as an argument and
                          allows user to launch\\
&&specified container
                          with the given capabilities \\
{\sc specify entrypoint}&& allows user to use a non-default entrypoint
  to\\
&&launch the specified container\\
{\sc inject process}&& allows user to execute arbitrary file in an already
                       running container\\
{\sc stop/start/restart}&&allows user to control container lifecycle\\
{\sc modify resource usage}&& takes a
                              \spec{resource\_constraint\_spec} as an
                              argument and allows user to launch\\
&&                              specified container with an altered
                              resource priority\\
\hline
\end{tabular}
\caption{\label{fig:policy_syntax}Syntax of our simplified presentation policy
  language. Our implementation uses a less succinct XML policy specification
  and supports all of these constraints and more.}
\end{figure*}

We will now describe some policies that can be expressed in \vanguard.
Our prototype implementation, which accepts policies written in an
ad-hoc XML format, can constrain a wide variety of options to the
Docker Remote API.  In this section, we highlight the most important
operations that \vanguard can constrain. We use the simplified,
declarative rule language shown in Figure~\ref{fig:policy_syntax} for
presentation purposes.


% \subsubsection{Identifying Principals}

% \vanguard policies can be specific to a particular user and to a particular 
% Docker application image, so we must have a way to identify each of these 
% principals.  

% As will be described in Section~\ref{sec:implement}, we leverage operating 
% system authentication in order to identify users in this
% implementation.  So a Linux user name or group name is used
% whenever a \vanguard policy refers a user. 

% Docker images are an ordered collection of layers, each of which is a
% set of file system changes along with some metadata.  Docker assigns
% a globally unique image ID to each layer.  Every image consists of a base image
% and zero or more additional layers, each of which stores the ID of the
% previous one as a parent pointer in its metadata.  Thus, specifying
% the ID of a single layer is sufficient to reconstruct an
% entire image by following the parent pointers all the way to the base
% image.  In addition, Docker allows users to assign one or more
% descriptive tags to an image.

% \vanguard policies can use either Docker image IDs or tags to refer to
% a specific image.  In addition, the \vanguard policy language allows
% administrators to apply a policy to all images that have a given image
% as an ancestor.  In this way, new images that are created when a user
% modifies an existing image can, if the administrator desires, 
% inherit policy rules already defined for the parent image. 

% Our current prototype places the \vanguard gateway process and the
% policies on an individual container host.  In a more complete
% implementation involving a policy server, the policy language could
% easily be extended with a host naming scheme so that, for instance,
% production container hosts could have more restrictive policies than
% hosts within the testing infrastructure.


% % \subsection{Container Policy Structure}
% % %
% % To simplify policy enforcement, \vanguard uses a policy file for each container 
% % image. The policy file specifies the docker operations that are allowed as well 
% % as parameters for the operations. Once a container is launched under the policy, 
% % the policy file and the use name will be attached to this container, any further 
% % operations on this container are subject to the policy file.

% % To clarify our policy design, we separate docker container policy into two parts: 
% % 1) container operation policy and image operation policy. This is because, 
% % container operation policy is per image based, while image management operation 
% % may not. In addition, for container operations, this policy further breaks down 
% % into container launch policy and container management policy, since container 
% % launch is much more complicated than the sum of the rest. The following shows 
% % the details of these two policies:

% \subsubsection{Policy Types}
% \label{sec:policy_type}

We divide the policy rules that \vanguard enforces into two categories.
\begin{itemize}
\item {\em Image Operation Policy (IOP)} These policies constrain
  Docker commands such as \code{pull}, \code{build}, and
  \code{push} that fetch, create, and store images.
\item {\em Container Operation Policy (COP)} These policies constrain
  Docker commands that create new containers and operate on existing
  containers.  Perhaps the most important of these
  policies are those that limit the Docker
  \code{run}  command which is used to launch a new container from an
  image and specifies which process is to be run within the container,
  which host resources will be made available to the container, and
  any special privileges that processes within the container will
  enjoy. 
% \item {\em Container Operation Policy (COP)} These policies constrain
%   various user commands that operate on existing containers, including
%   commands that pause and restart containers and advanced commands
%   that allow new processes to be injected into a running container.
\end{itemize}
% As shown in Figure~\ref{fig:vanguard_arch}, container operation
% policies are specific to a particular user and a particular image,
% while image management policies specify only a user.


\newcommand{\policydescstart}{\begin{itemize}}
\newcommand {\policycmd}[2]{\item {\smallcode{#1}} \\ #2}
\newcommand{\policydescend}{\end{itemize}}
% \newcommand{\policydescstart}{\begin{tabular}{ p{.45\textwidth} }}
% \newcommand {\policycmd}[2]{\bf{#1} \\ #2 \\\\}
% \newcommand{\policydescend}{\end{tabular}}
\newcommand{\unm}{\textit{user\_name}\xspace}
\newcommand{\imgl}{\textit{image\_list}\xspace}
\newcommand{\rscl}{\textit{resource\_list}\xspace}
\newcommand{\usrl}{\textit{user\_list}\xspace}

%
\subsubsection{Image Operation Policy}
\label{sec:iop}
%
% \vanguard allows an administrator to specify an image operation policy
% for each user.  Here are some example image operation policy rules.

\vanguard allows an administrator to specify which images can be
downloaded from registries, and by which users.  For instance:
\policydescstart %\begin{description}
\policycmd{ALLOW ALL PULL hub.docker.com/*}{Grant all users the
  ability to pull any image from the official public Docker registry.}
\policycmd{DISALLOW ALL PULL hub.docker.com/vulnerable-app}{But, you
  may want to disallow anyone from downloading certain apps (e.g., a
  web server) with known vulnerabilities.}
\policydescend %\end{description}
Normally, we imagine that an administrator would create a global
whitelist of images that all users are allowed to download, but more
fine-grained policies are possible.  For instance, a trusted developer
may be given permission to pull arbitrary images from the docker hub,
but operations personnel may only download and launch a few production 
images from an internal registry.

An administrator may also want to constrain who is able to modify
images in order to create new images.  For instance:
\policydescstart %\begin{description}
\policycmd{ALLOW developers EXTEND FROM hub.docker.com/*}{Grants
  users in the ``developers'' group the permission to build new
  images based on containers launched from official Docker registry images.}
\policydescend %\end{description}
In our current implementation, we use operating system users and
groups for authentication and to establish identity.  
This could easily be replaced by another authentication or access control mechanism.




% \paragraph{Image Name Policy} To avoid maintaining a list of authorized images 
% for each user, \vanguard simply uses image name space by enforcing the image 
% name in the format of {\tt user\_name/image\_name}. For any operations, 
% \vanguard will check if an image is allowed by checking whether the user name 
% matches or not.

% Both {\tt docker build} and {\tt docker tag} operations could generate new 
% image. A potential issue is that a new image could override the official name 
% of an image and image of other users.. By doing so, a normal user may launch 
% attacks to force other users to execute his own images.

% To prevent this, \vanguard enforces an image naming policy. Any image newly 
% generated must follow the name format: {\tt user\_name/image\_name} or  {\tt 
% user\_name/image\_name\\}{\tt :tag\_name}. Since each user will have his own name, the 
% new name won't overwrite any images that the user is not authorized to rename.

% \paragraph{New Image Generation} Another issue is that {\tt docker build} 
% operation will generate a new image. It builds a new image following instruction 
% in a file called {\tt Dockerfile}. There are two problems here: 1) source image 
% may be unauthorized. 2) target image does not have a policy.

% For the 1st problem, \vanguard makes sure that the source image must be 
% authorized. This is achieved by checking the image name format as mentioned in 
% previous paragraph.

% To allow any new image to be properly used by the user, \vanguard automatically 
% generates a new policy file for each user on this image. \vanguard achieves this 
% by intercepting image generation operation. And by analyzing the {\tt 
% Dockerfile} provided, \vanguard is able to gather potential new entry point, 
% volume and port information. The new policy will inherit the one in base image.

% However, there is on exception in policy inheritance. The allowed data resource 
% mapping cannot be inherited in the new image. (e.g.: directory {\tt /data} in 
% host is allowed to be mapped when base image A is used. However, new image B 
% generated from A cannot map {\tt /data}. This is to prevent attacks that try
% to bypass policies. We will discuss it further in Section~\ref{sec:app}

%
\subsubsection{Container Operation Policy}
%
Creating a container from an image via the Docker \code{run} or \code{create}
command is an operation that is essential to constrain.  As
mentioned in Section~\ref{sec:background}, container processes are
forked from the Docker daemon which runs with root privileges.  The
options included in the \code{run} or \code{create} command line
determine what privileges, if any, the newly launched process
relinquishes.  \vanguard's container launch policies constrain these
sensitive commands, as well as other commands affecting the
operation of an existing container. 

\vanguard allows users to run any image that the administrator has
permitted them to download.  In addition, once a user creates a new
image, he is able to run it.  In a more complete implementation, we
envision that users would be able to specify other users or groups of
users that are allowed to run the images that they create, rather than
relying on an administrator to update the policy.

By default, \vanguard ensures that
a user is not able to launch a container with more privileges than
the user is granted by the operating system.  
For instance, 
\vanguard enforces that a user can mount host data into a container
only if the user has file system permissions to that data.  Similarly,
\vanguard checks any attempt to launch a container with a specific 
operating system privilege to ensure that the user initiating the
operation has the privilege. An
administrator may, however, grant additional privileges to specific
users when launching specific images.  For instance:
\policydescstart
\policycmd
{ALLOW qa MAP HOST FILE /private/website FOR apache}
{Allow users in the ``qa'' group to launch a container from the apache
  web server image and give it access to directory /private/website.}
\policycmd
{ALLOW qa GRANT CAPABILITY CAP\_NET\_BIND\_SERVICE FOR apache}
{Grants users in the ``qa'' group the permission to launch a container
  from the apache image with a specific operating system capability,
  in this case the Linux {\footnotesize CAP\_NET\_BIND\_SERVICE} which allows binding to a
  privileged port.}
\policydescend

The administrator in the previous example has granted the QA users the
ability to launch Apache with access to a privileged directory
containing the website.  But, following the principle of least
privilege, she may not want these users to have access to some private
PHP scripts within that directory.  Unfortunately, Docker allows launching
a container from an image with a non-default entry
point.  For instance, the command\\
\smallcode{\$ docker run -v /private/website:/var/www \textbackslash
  \\
  > -v \textasciitilde:/myhome apache cp  -r /var/www /myhome/} \\
launches the apache image, gives it access to
both the private website directory and the user's home directory.  But
then, instead of executing the default \code{httpd} process, it 
copies the private website directory into the user's home
directory.  In order to prevent circumventing the intended policy in
this way, \vanguard disallows launching images with non-default
entrypoints unless it is explicitly allowed by a policy. 
 A similar issue arises with the Docker
\code{exec} command, which allows a user to run a new process within
an existing container.  Privileges to use these Docker features can be
granted to specific users for specific images.  For instance:
\policydescstart 
\policycmd 
{ALLOW developer SPECIFY ENTRYPOINT FOR ALL} 
{Grants users in the ``developer'' group the ability to specify
  a non-standard entrypoint for all images which they are permitted to
  run.}  
\policycmd
{ALLOW developer INJECT PROCESS FOR ubuntu}
{Grants users in the ``developer'' group the ability to issue
  \code{exec} commands to inject processes into containers launched
  from the ubuntu image.}
\policydescend

Finally, \vanguard policies can grant privileges on the ability to
stop and start containers launched by others.  For instance:
\policydescstart
\policycmd
{ALLOW ops RESTART FOR ALL::ALL}
{Grants users in the ``ops'' group the ability to stop and start
  containers launched by all other users.}
\policydescend


% \paragraph{Image Name} Container images that contain malicious binaries should 
% never be run. \vanguard avoids the security problem by enforcing a white list of 
% authorized images. This is enforced by image name policy mentioned in Section~\ref{sec:iop}

% %Note that recent docker public registry supports a new feature called trusted 
% %build~\cite{trusted-build}, which allows automatic build of an image from a 
% %{\tt github} project. To improve usability, \vanguard could be configured to accepts 
% %all such images. This is achieved by checking metadata information of the image. 
% %
% %However, there is no guarantee that any of these images would be 
% %fully trusted. To handle any potential security issues due to malicious binaries, 
% %\vanguard could benefit from binary scanning services delivered in containers. 
% %It works as follows: \vanguard first downloads the target image and then launches a 
% %virus scanner container that maps the target image layers into the container and 
% %performs a virus checking. If there is any malicious binaries detected, the 
% %whole image will be deleted and further download attempt on this image will be 
% %rejected.
% %
% \paragraph{Entry point} Entry point is the intended program to launch a 
% container. Containers usually contain an entry point so that it can be delivered 
% as if they are executable binaries. Docker provides a primitive {\tt ENTRYPOINT} 
% to specify the entry point of a container when used for build container image, 
% {\tt ENTRYPOINT} primitive will enforce the startup program to be the one 
% specified unless changing entry point is explicitly specified (with {\tt 
% --entrypoint}) in container launch parameters. Alternatively, Docker provides an 
% additional primitive {\tt CMD} to specify a default entry point program. The 
% difference is that this program can be changed 
% if launch operation specify another startup command.

% As we will show in our evaluation in Section~\ref{sec:img-anal}, most of the docker 
% container images are executable images, i.e. only one entry point program is 
% intended to launch a container. \vanguard gathers all possible entry points of a 
% container by checking the metadata of its image. In particular, \vanguard uses 
% the docker registry API to gather the metadata of each image and by checking 
% this metadata, the system identifies all possible entry points of an image.
% %
% \paragraph{Resource Mapping} Docker command line provide options to allow containers map 
% resource of the host into them. There are two type of resource in the 
% host: data and network ports. For data mapping, \vanguard ensures two 
% properties: 1) code of the host will never be mapped into container and 2) 
% configuration files and personal metadata won't be mapped into container.
% Both of the these properties are enforced by a path checking of whitelisted path 
% names. For port mapping, \vanguard prevent any privileged ports (port from 1 
% to 1024) to be mapped into containers.

% Except resource mapping such as data and port from the host, there is another 
% type of resource mapping, i.e. resource mapping from running containers. Docker 
% support this with two parameters: {\tt--link } and {\tt --volume-from}.

% \vanguard supports this type of resource mapping if the target container is 
% started by the same user. This however, prevent resource sharing among users. We 
% put this part as our future work.

% %
% \subsubsection{Container Operation Policy}
% %
% Once a container has been created and launched, various commands can
% be used to affect its operation.  \vanguard container operation
% policies constrain the use of these commands.  We describe a few
% examples here.


% \policydescstart %\begin{description}
% \policycmd{ALLOW \usrl INJECT TO \imgl
%   OWNED BY \usrl}{Grants
%   users the permission to issue the \texttt{exec} command on containers
%   that were launched by one of the specified owners from the specified
%   image list.}
% \policycmd{ALLOW \usrl RESTART \imgl OWNED BY \usrl}{Grants
%   users the permission to pause and restart containers that were
%   launched by one of the specified owners from the specified image list.}
% \policydescend %\end{description}


% \paragraph{Container Access} Since docker v1.3, injecting a new process inside 
% an existing container is allowed. This is implemented by command {\tt docker 
% exec}. \vanguard limit this capability to certain users. This is because most of 
% the users do not need such privilege, since the major purpose is to debug an 
% existing container. Moreover, if these accounts get corrupted, attacker may 
% natively access critical containers that has important data mounted. To 
% constrain this capability, vanguard allows developers and testers to inject a 
% new process into existing containers. For normal users, process injection is 
% prohibited. 

% \paragraph{Other Container Operations} Other container operations such as 
% container attach, stop, pause and remove are also constrained by \vanguard. For 
% local users, \vanguard basically  allows the owner of the container to perform 
% these operations on default.
